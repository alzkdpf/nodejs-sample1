import bcrypt from "bcrypt";

const password = "1234";
const hashed = bcrypt.hashSync(password, 10);
console.info(`hashed: ${hashed} / password: ${password}`);
