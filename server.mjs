import express from "express";
import createError from "http-errors";
import Next from "next";
import cookieParser from "cookie-parser";
import logger from "morgan";
import helmet from "helmet";
import twitterRouter from "./server/twitter/router/tweet.mjs";
import authRouter from "./server/twitter/router/auth.mjs";
import { config } from "./config.mjs";
import cors from "cors";

// env settings
const dev = true;
const hostname = "localhost";
const port = config.host.port;

const nextJsEnabled = true;

const nextJs = Next({ dev, hostname, port });
const nextJsRequestHandler = nextJs.getRequestHandler();

(async () => {
  try {
    if (nextJsEnabled) await nextJs.prepare();

    const expressServer = express();

    expressServer.use(logger("dev"));
    expressServer.use(express.json());
    expressServer.use(helmet());
    expressServer.use(express.urlencoded({ extended: false }));
    expressServer.use(cookieParser());
    expressServer.use(cors());

    expressServer.set("trust proxy", true);

    expressServer.use("/api/tweets", twitterRouter);
    expressServer.use("/api/auth", authRouter);

    if (nextJsEnabled)
      expressServer.get("*", (req, res) => {
        return nextJsRequestHandler(req, res);
      });

    expressServer.listen(port, (err) => {
      if (err) throw err;
      console.info(`http://localhost:${port}`);
    });

    expressServer.use(function (req, res, next) {
      next(createError(404));
    });

    // error handler
    expressServer.use(function (err, req, res, next) {
      // set locals, only providing error in development
      res.locals.message = err.message;
      res.locals.error = req.app.get("env") === "development" ? err : {};

      // render the error page
      res.status(err.status || 500);
      res.render("error");
    });
  } catch (ex) {
    console.error(ex.stack);
    process.exit(1);
  }
})();
